const express = require('express');
const fs = require('fs');
const path = require('path');

const router = express.Router();

router.get('/', (req, res) => {
  const directoryPath = path.join(__dirname, '../uploads');

  fs.readdir(directoryPath, (err, files) => {
    if (err) {
      return res.status(500).send({
        message: 'Unable to scan files!',
        error: err
      });
    }

    const fileNames = files.map(file => {
      return path.basename(file);
    });

    res.send({
      files: fileNames 
    });
  });
});

module.exports = router;
