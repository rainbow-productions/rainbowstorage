const express = require('express');
const multer = require('multer');
const path = require('path');

const uploadRouter = require('./api/upload');
const filesRouter = require('./api/files');

const app = express();

const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, './uploads');
  },
  filename: function(req, file, cb) {
    cb(null, file.originalname);
  }
});

const upload = multer({ storage });

app.use('/api/upload', uploadRouter);
app.use('/api/files', filesRouter);

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, 'public/index.html'));
});

app.get('/files', (req, res) => {
  res.sendFile(path.join(__dirname, 'public/files.html')); 
});

app.listen(3000, () => {
  console.log('Server listening on port 3000');
});
